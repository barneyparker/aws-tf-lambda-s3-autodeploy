console.log('Loading function');
var AWS = require('aws-sdk');
var lambda = new AWS.Lambda();

exports.handler = function(event, context) {
  const bucket  = event.Records[0].s3.bucket.name
  const key     = event.Records[0].s3.object.key
  const version = event.Records[0].s3.object.versionId

  console.log('Bucket: ', bucket)
  console.log('Key: ', key)
  console.log('Version: ', version)

  if (bucket == process.env.bucket && key == process.env.key && version) {
    const functionName = process.env.function_name;

    console.log("uploaded to lambda function: " + functionName);

    let params = {
      FunctionName: functionName,
      S3Key: key,
      S3Bucket: bucket,
      S3ObjectVersion: version
    };

    lambda.updateFunctionCode(params,(err, data) => {
      if (err) {
        console.log(err, err.stack);
        context.fail(err);
      } else {
        console.log(data);
        context.succeed(data);
      }
    });

    params = {
      FunctionName: functionName
    }

    lambda.getFunctionConfiguration(params, (err, data) => {
      if(err) {
        console.log("Error getFunctionConfiguration: ", err)
        context.fail(err);
      } else {
        console.log("Current Function Configuration: ", data)

        params.Environment = {
          Variables: data.Environment.Variables
        }

        params.Environment.Variables.provider = "lambda-deployment"
        params.Environment.Variables.timestamp = getDateTime()

        lambda.updateFunctionConfiguration(params, (err, data) => {
          if(err) {
            context.fail(err)
          } else {
            context.succeed(data)
          }
        })
      }
    })
  } else {
    context.succeed("skipping zip " + key + " in bucket " + bucket + " with version " + version);
  }
};

function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}
