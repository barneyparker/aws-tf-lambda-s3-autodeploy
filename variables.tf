variable "target_bucket" {
  description = "Bucket id to watch for upload events"
}

variable "target_bucket_arn" {
  description = "Bucket ARN to watch for upload events"
}

variable "target_zip" {
  description = "Name of the .zip file containing lambda functions (stored in the S3 bucket)"
}

variable "target_function_name" {
  type = "string"
  description = "The function name of the Lambda to be deployed"
}

variable "target_function_arn" {
  type = "string"
  description = "ARN of the function we're deploying to"
}
