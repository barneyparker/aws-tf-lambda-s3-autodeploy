module "deployer" {
  source = "bitbucket.org/barneyparker/aws-tf-lambda-dir"

  source_dir = "${path.module}/src"
  lambda_name = "lambda-deployer-demo"
  handler = "index.handler"
  runtime = "nodejs8.10"

  environment = {
    bucket = "${var.target_bucket}"
    key = "${var.target_zip}"
    function_name = "${var.target_function_name}"
  }
}

resource "aws_iam_role_policy" "update_target_function" {
  name = "update_target"
  role = "${module.deployer.role_id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "lambda:UpdateFunctionCode",
        "lambda:GetFunctionConfiguration",
        "lambda:UpdateFunctionConfiguration"
      ],
      "Resource": [ "${var.target_function_arn}" ]
    },
    {
      "Effect": "Allow",
      "Action": [ "s3:Get*", "s3:List*" ],
      "Resource": [ "${var.target_bucket_arn}", "${var.target_bucket_arn}/*" ]
    }
  ]
}
EOF
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${module.deployer.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${var.target_bucket_arn}"
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${var.target_bucket}"

  lambda_function {
    lambda_function_arn = "${module.deployer.arn}"
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = "${var.target_zip}"
  }
}
